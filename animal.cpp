﻿// animal.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;

class Animal
{
public:
  virtual const char* Voice()
   {
       return "What?";
   }

};

class Cat : public Animal
{
public:
    const char* Voice() override
    {
        return "Meow!";
    }
};

class Dog : public Animal
{
public:
    const char* Voice() override
    {
        return "Woof!";
    }
};

class Owl : public Animal
{
public:
    const char* Voice() override
    {
        return "Hoooo!";
    }
};

class Goose : public Animal
{
public:
    const char* Voice() override
    {
        return "Honk!";
    }
};

class Bear : public Animal
{
public:
    const char* Voice() override
    {
        return "Roar!";
    }
};

class Bee : public Animal
{
public:
    const char* Voice() override
    {
        return "Buzz!";
    }
};

int main()
{
    Owl owl;
    Cat cat;
    Dog dog;
    Bee bee;
    Goose goose;
    Bear bear;

    Animal* pAnimal[] = { &cat, &dog, &owl, &bear, &goose, &bee };

    for (int i = 0; i < 6; i++)
    {
        cout << pAnimal[i]->Voice() << "\n" << endl;
    }


    

   
}